import numpy as np

import matplotlib.pyplot as plt

import pandas as pd
import constants as cs
import aux_func as aux

def get_viz(log_path, borders):

        df = pd.read_csv(log_path + "tracks.csv")

        agent_list = df.agent_id.unique()

        fig, axs = plt.subplots(3, 2, figsize = [15,10])


        for agent_id in agent_list:

            subset = df[df["agent_id"] == agent_id]

            axs[0,0].set_title("Time Relaxation Force vs. Time Step")

            axs[0,0].plot(subset["step"], subset["time_relaxation_mag"])
            axs[0,0].set_xlabel("Time Step (dt)")
            axs[0,0].set_ylabel("Time Relaxation")

            axs[0,1].set_title("Agent Repulsion vs. Time Step")

            axs[0,1].plot(subset["step"], subset["agent_repulsion_mag"])
            axs[0,1].set_xlabel("Time Step (dt)")
            axs[0,1].set_ylabel("Agent Repulsion")

            axs[1,0].set_title("Resultant vs. Time Step")

            axs[1,0].plot(subset["step"], subset["resultant_mag"])
            axs[1,0].set_xlabel("Time Step (dt)")
            axs[1,0].set_ylabel("Resultant")


            axs[1,1].set_title("Velocity vs. Time Step")

            axs[1,1].plot(subset["step"], subset["velocity_mag"])
            axs[1,1].set_xlabel("Time Step (dt)")
            axs[1,1].set_ylabel("Velocity")


            axs[2,1].set_title("Border Repulsion")

            axs[2,1].plot(subset["step"], subset["border_repulsion_mag"])
            axs[2,1].set_xlabel("Step (dt)")
            axs[2,1].set_ylabel("Border Repulsion")

        plt.tight_layout()

        fig.savefig(log_path + "grid.png", dpi = 300)
        plt.close()

        fig, ax = plt.subplots()


        for agent_id in agent_list:

            subset = df[df["agent_id"] == agent_id]
            ax.set_title("Trajectories")

            ax.scatter(subset["position_x"], subset["position_y"], s = 1)
            ax.set_xlabel("Position x")
            ax.set_ylabel("Position y")
            ax.axis('equal')
            ax.hlines(cs.DIST_THRESH, xmin = 0, xmax = cs.X, linestyles = "dashed")
            ax.hlines(cs.Y - cs.DIST_THRESH, xmin = 0, xmax = cs.X, linestyles = "dashed")

        for border in borders:
            Q = np.array([border.Qx, border.Qy]).T
            ax.plot(Q[:,0], Q[:,1], linewidth = 1, color = "black")

        fig.savefig(log_path + "trajs.png", dpi = 300)
        plt.close()
