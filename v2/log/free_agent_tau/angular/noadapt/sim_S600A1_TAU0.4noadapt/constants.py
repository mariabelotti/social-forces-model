import numpy as np
import pandas as pd

## The following lines define some constants that are used in the model.


## The next two constants define numerical deltas. They were set to produce smooth curves. 
# Numerical time step lenght:
delta_t = 0.01

# Defines numerical "space" step to calculate gradients:
DS = 0.01

#-----------------------------------------------------#

# Number of agents:
N = 1

# Number of time steps:
M = 600

# Relaxation time:
TAU = 0.5

# Defines environment size:
X = 10 # Defines size corridor lenght
Y = 10 # Defines corridor width

# Agent's maximal speed is set by multiplying the desired speed by the following factor:
MAX = 1.3 

# Defines half the angle of view, in radian:
PHI = 1.74

# Defines angle of view attenuation: 
C = 0.5 

# Constants used to calculate repulsion potential between agents:
V_ZERO = 2.1
SIGMA = 0.3

# Constants used to calculate repulsion potential from borders:
U_ZERO = 1000
R = 0.2

# Distante threshold used to ease computations:
DIST_THRESH = 2

# Border definition:
BORDERS = pd.read_csv("./borders_simple.csv")

# Defines the ability of the pedestrian to foresee where the pedestrians around her will be in the future:
Rt = 0.1

#-----------------------------------------------------#

# Agent's initial speed is chosen from a normal distribution with the following mean and standard deviation:
DESIRED_SPEED_loc = 1.34
DESIRED_SPEED_scale = 0.26 

# Fluctuaion of the resulting force is chosen from a distribution with the following mean and standard deviation:
FLUCT_loc = 1 
FLUCT_scale = 1  

# Agent's desired position (when this can be set globally):
DESIRED_POSITION = np.array([7,5])

#-----------------------------------------------------#
