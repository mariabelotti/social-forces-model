import numpy as np

from constants import *
import aux_func as aux


class Border:
    """Borders are defined either as function of x or as a set of coordinated pairs that can be generated however the user wants) """

    def __init__(self, uid, Qx, Qy):

        self.Qx = Qx
        self.Qy = Qy
        self.uid = uid

    def calc_dist(self, Px, Py, uid):
        """ Calculates the distance betweeen an agent in position (Px, Py) and the border with given uid. """

        Q = np.array([self.Qx, self.Qy]).T

        border_size = len(Q)
        P = np.repeat(np.array([[Px, Py]]), repeats = border_size, axis = 0)

        dists = Q-P

        norm_dists = []

        for dist in dists:
            dist = aux.norm(dist)
            norm_dists.append(dist)

        norm_dists = np.array(norm_dists)
        ind = np.argmin(norm_dists)

        return dists[ind]
