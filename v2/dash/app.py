# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html

from dash.dependencies import Input, Output

import numpy as np

import plotly.graph_objects as go
import plotly.express as px

import pandas as pd

sim_code1 = "0523/sim_S500A50_1055"
sim_code2 = "0522/sim_S50A2_1524"
sim_code3 = "0522/sim_S50A2_1521"
file_list = [sim_code1, sim_code2, sim_code3]

data_file1 = "/home/maria/current/work/poli/masters_project/model/SocialForces/log/" + sim_code1 + "/tracks.csv"
data_file2 = "/home/maria/current/work/poli/masters_project/model/SocialForces/log/" + sim_code2 + "/tracks.csv"
data_file3 = "/home/maria/current/work/poli/masters_project/model/SocialForces/log/" + sim_code3 + "/tracks.csv"



df1 = pd.read_csv(data_file1)
df2 = pd.read_csv(data_file2)
df3 = pd.read_csv(data_file3)

X = 20
Y = 20

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']


app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


app.layout = html.Div([
        
        html.H1("Simulação: Forças Sociais, Helbing (1995)"),
        html.Div([
            dcc.Dropdown(
                id = "dataset",
                options=[{'label': i, 'value': i} for i in file_list],
                value= file_list[0],
                multi = False
            )  
        ]),

        html.Div(id='current'),

        # # html.Div([
        # #     dcc.Graph(
        # #         id="animation"
        # #     )
        # ]),
        
        html.Div([    
        
            html.Div([
                dcc.Graph(
                    id='resultant_mag'
                ),
                dcc.Graph(
                    id='agent_repulsion_mag'
                ),
                dcc.Graph(
                    id='time_relaxation_mag'
                )

            ], className="one-half column"),

            html.Div([
                dcc.Graph(
                    id='velocity_mag'
                ),
                dcc.Graph(
                    id='border_repulsion_mag'
                ),
                dcc.Graph(
                    id='position'
                ) 
            ], className="one-half column"),
        ], className="row")
    ])




# @app.callback(
#     Output(component_id = 'current', component_property = 'children'),
#     [Input(component_id = 'dataset', component_property= 'value')])
# def update_graph(new_data):
#     return new_data

@app.callback(
    [Output(component_id = 'resultant_mag', component_property = 'figure'),
    Output(component_id = 'velocity_mag', component_property = 'figure'),
    Output(component_id = 'agent_repulsion_mag', component_property = 'figure'),
    Output(component_id = 'border_repulsion_mag', component_property = 'figure'),
    Output(component_id = 'time_relaxation_mag', component_property = 'figure'),
    Output(component_id = 'position', component_property = 'figure'),
    Output(component_id = 'current', component_property = 'children')],
    [Input(component_id = 'dataset', component_property= 'value')])
def update_graph(new_data):
    if new_data == file_list[0]:
        df = df1
        string = "Você está vendo a simulação de código: " + file_list[0]

    elif new_data == file_list[1]:
        df = df2
        string = "Você está vendo a simulação de código: " + file_list[1]
    
    elif new_data == file_list[2]:
        df = df3
        string = "Você está vendo a simulação de código: " + file_list[2]

    agent_list = df.agent_id.unique()

    fig1 = go.Figure()
    fig2 = go.Figure()
    fig3 = go.Figure()
    fig4 = go.Figure()
    fig5 = go.Figure()
    fig6 = go.Figure()
    # fig7 = go.Figure()

    for agent_id in agent_list:
        subset = df[df["agent_id"] == agent_id]

        fig1.add_trace(go.Scatter(x = subset["step"], y = subset["resultant_mag"]))

        fig2.add_trace(go.Scatter(x = subset["step"], y = subset["velocity_mag"]))

        fig3.add_trace(go.Scatter(x = subset["step"], y = subset["agent_repulsion_mag"]))

        fig4.add_trace(go.Scatter(x = subset["step"], y = subset["border_repulsion_mag"]))

        fig5.add_trace(go.Scatter(x = subset["step"], y = subset["time_relaxation_mag"]))

        fig6.add_trace(go.Scatter(x = subset["position_x"], y = subset["position_y"], hovertext = subset["step"]))

        # fig7 = px.scatter(df, x="position_x", y="position_y", animation_frame="step",
        #        color="agent_id",
        #        template = "ggplot2", size_max=55, range_x=[0,X], range_y=[0,Y])

        fig1.update_layout(
        title = "Resultante em Função de dt",
        xaxis_title = "Tempo (dt)",
        yaxis_title = "Magnitude - Resultante", 
        template = "ggplot2")

        fig2.update_layout(
        title = "Velocidade em Função de dt",
        xaxis_title = "Tempo (dt)",
        yaxis_title = "Magnitude - Velocidade", 
        template = "ggplot2")

        fig3.update_layout(
        title = "Repulsão entre Agentes em Função de dt",
        xaxis_title = "Tempo (dt)",
        yaxis_title = "Magnitude - Repulsão entre Agentes", 
        template = "ggplot2")

        fig4.update_layout(
        title = "Repulsão em Relação às Bordas em Função de dt",
        xaxis_title = "Tempo (dt)",
        yaxis_title = "Magnitude - Força de Repulsão", 
        template = "ggplot2")

        fig5.update_layout(
        title = "Força de Relaxamento em Função de dt",
        xaxis_title = "Tempo (dt)",
        yaxis_title = "Magnitude - Força de Relaxamento", 
        template = "ggplot2")

        fig6.update_layout(
        title = "Trajetórias",
        xaxis_title = "Posição x",
        yaxis_title = "Posição y", 
        template = "ggplot2")


    return fig1, fig2, fig3, fig4, fig5, fig6, string


if __name__ == '__main__':
    app.run_server()