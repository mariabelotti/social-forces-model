################################
## Scenario #5 Experiments    ##
################################

# Number of agents:
N = 63

# Number of time steps:
M = 3000

# Defines environment size:
X = 48 # Defines size corridor lenght
Y = 3 # Defines corridor width

#####################
## Configuration A: #
#####################


            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False
            
            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            #X_pos2 = np.linspace(start=36, stop=48, num=int(np.floor(cs.N/12)))
            #Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0 

            for x in X_pos1:
                for y in Y_pos1:

                    position = np.array([x,y])                
                    desired_direction = np.array([1,0])

                    agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                    self.agents.append(agent)
                    uid = uid + 1




#####################
## Configuration A: #
#####################


            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False
            
            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            #X_pos2 = np.linspace(start=36, stop=48, num=int(np.floor(cs.N/12)))
            #Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0 

            for x in X_pos1:
                for y in Y_pos1:

                    position = np.array([x,y])                
                    desired_direction = np.array([1,0])

                    agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                    self.agents.append(agent)
                    uid = uid + 1

#####################
## Configuration B: #
#####################

            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False
            
            occupation = np.ones([9,6])

            occupation[0,3] = 0
            occupation[1,2] = 0
            occupation[2,5] = 0
            occupation[3,0] = 0
            occupation[4,4] = 0
            occupation[5,3] = 0
            occupation[6,1] = 0
            occupation[7,4] = 0
            occupation[8,3] = 0
            
            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0 

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:
                        
                        position = np.array([X_pos1[i],Y_pos1[j]])                
                        desired_direction = np.array([1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

            occupation = np.zeros([9,6])

            occupation[0,5] = 1
            occupation[1,1] = 1
            occupation[2,0] = 1
            occupation[3,4] = 1
            occupation[4,2] = 1
            occupation[5,2] = 1
            occupation[6,5] = 1
            occupation[7,3] = 1
            occupation[8,1] = 1
            
            X_pos2 = np.linspace(start=36, stop=48, num=9)
            Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:
                        
                        position = np.array([X_pos2[i],Y_pos2[j]])                
                        desired_direction = np.array([-1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1


#####################
## Configuration C: #
#####################

            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False
            
            occupation = np.ones([9,6])

            occupation[0,[3,2]] = 0
            occupation[1,[2,4]] = 0
            occupation[2,[5,4]] = 0
            occupation[3,[2,3]] = 0
            occupation[4,[2,4]] = 0
            occupation[5,[1,5]] = 0
            occupation[6,[2,4]] = 0
            occupation[7,[2,5]] = 0
            occupation[8,[1,4]] = 0

            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0 

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:
                        
                        position = np.array([X_pos1[i],Y_pos1[j]])                
                        desired_direction = np.array([1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

            occupation = np.zeros([9,6])

            occupation[0,[2,5]] = 1
            occupation[1,[0,3]] = 1
            occupation[2,[1,4]] = 1
            occupation[3,[4,5]] = 1
            occupation[4,[0,3]] = 1
            occupation[5,[1,2]] = 1
            occupation[6,[1,5]] = 1
            occupation[7,[0,3]] = 1
            occupation[8,[1,4]] = 1
            
            X_pos2 = np.linspace(start=36, stop=48, num=9)
            Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:
                        
                        position = np.array([X_pos2[i],Y_pos2[j]])                
                        desired_direction = np.array([-1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1



#####################
## Configuration D: #
#####################

            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False
            
            occupation = np.zeros([9,6])

            occupation[0,[0,2,5]] = 1
            occupation[1,[0,1,4]] = 1
            occupation[2,[2,4,5]] = 1
            occupation[3,[0,1,3]] = 1
            occupation[4,[0,3,5]] = 1
            occupation[5,[1,3,5]] = 1
            occupation[6,[0,1,4]] = 1
            occupation[7,[0,2,5]] = 1
            occupation[8,[1,4,5]] = 1

            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0 

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:
                        
                        position = np.array([X_pos1[i],Y_pos1[j]])                
                        desired_direction = np.array([1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

            occupation = np.zeros([9,6])

            occupation[0,[0,1,4]] = 1
            occupation[1,[0,3,5]] = 1
            occupation[2,[0,1,4]] = 1
            occupation[3,[1,2,5]] = 1
            occupation[4,[2,4,5]] = 1
            occupation[5,[0,3,5]] = 1
            occupation[6,[0,2,4]] = 1
            occupation[7,[3,4,5]] = 1
            occupation[8,[0,4,5]] = 1
            
            X_pos2 = np.linspace(start=36, stop=48, num=9)
            Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:
                        
                        position = np.array([X_pos2[i],Y_pos2[j]])                
                        desired_direction = np.array([-1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1