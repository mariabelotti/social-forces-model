import numpy as np

import constants as cs
import aux_func as aux

class Agent:

    # Class attributes:

    # Initializer to define instance attributes:
    def __init__(self, uid, position, desired_direction, from_zero, adapt, turns):
        self.uid = uid # agent's unique id
        self.position = position # agent's current position

        self.desired_speed = np.random.normal(loc = cs.DESIRED_SPEED_loc, scale = cs.DESIRED_SPEED_scale)
        #self.desired_speed = 1.34
        self.max_speed = cs.MAX * self.desired_speed # agent's maximal speed

        # All agents start in active status, which means they will be moved:
        self.status = True

        # triggers adaptable desired position:
        if adapt == True:
            self.desired_position = cs.DESIRED_POSITION
            self.desired_direction = self.get_desired_direction()
            self.adapt = True
        else:
            self.desired_direction = desired_direction
            self.adapt = False

        # triggers initial speed starting from zero:
        if from_zero == True:
            initial_speed = 0
            self.velocity = np.array([initial_speed*desired_direction[0], initial_speed*desired_direction[1]])
        else:
            self.velocity = np.array([self.desired_speed*desired_direction[0], self.desired_speed*desired_direction[1]])

        # triggers turning behavior, won't work together with from_zero:
        if turns == True:
            initial_direction = np.array([0,1])
            self.velocity = self.desired_speed*initial_direction
        else:
            self.velocity = np.array([self.desired_speed*desired_direction[0], self.desired_speed*desired_direction[1]])

        self.resultant = []
        self.track = []


    def get_desired_direction(self):
        """Calculates the vector that indicates the desired direction of movement, based on the agent's
        current position and where she wants to go."""
        ex = self.desired_position[0] - self.position[0]
        ey = self.desired_position[1] - self.position[1]
        norm_e = aux.norm([ex,ey])
        return np.array([ex/norm_e, ey/norm_e])


    def calc_agent_repulsion(self, x, y, v_beta, v_alpha):

        v_rel = v_beta - v_alpha

        d = np.array([x,y])
        p = aux.norm(d)
        q = aux.norm(d - v_rel*cs.Rt)

        f = aux.norm(v_rel*cs.Rt)

        b = np.sqrt((p+q)**2 - f**2)/2

        if b == 0:
            b = 0.01

        if q == 0:
            q = 0.01

        fator1 = cs.V_ZERO*np.exp(-b/cs.SIGMA)
        fator2 = (p + q)/(4*b)
        fator3 = (d/p) + (d - v_rel*cs.Rt)/q

        F = fator1*fator2*fator3

        return np.array(F)


    def calc_border_repulsion(self, x, y):
        ''' This function receives the distance betweeen border and agent, and returns the repulsive force between agent and border.'''

        if (x == 0) and (y == 0):
            Fx = 0
            Fy = 0

        else:
            Fx = -(cs.U_ZERO*x*np.exp(-(aux.norm([x,y]))/cs.R))/(cs.R*aux.norm([x,y]))
            Fy = -(cs.U_ZERO*y*np.exp(-(aux.norm([x,y]))/cs.R))/(cs.R*aux.norm([x,y]))

        return np.array([Fx, Fy])


    # def gradient(self, pot, x, y):

    #     dist_x = np.arange(start=-2*cs.DIST_THRESH, stop=2*cs.DIST_THRESH, step=cs.DS)
    #     dist_y = np.arange(start=-2*cs.DIST_THRESH, stop=2*cs.DIST_THRESH, step=cs.DS)

    #     Fx, Fy = np.gradient(pot)

    #     nx = self.find_nearest(dist_x, x)
    #     ny = self.find_nearest(dist_y, y)

    #     return np.array([Fx[nx, ny], Fy[nx, ny]])

    # def find_nearest(self, array, value):
    #     idx = (np.abs(array - value)).argmin()
    #     return idx


    # def V(self, ex, ey, v_beta):
    #     '''This function gives the repulsive potential between agents.'''

    #     dist_x = np.arange(start = -2*cs.DIST_THRESH, stop = 2*cs.DIST_THRESH, step = cs.DS)
    #     dist_y = np.arange(start = -2*cs.DIST_THRESH, stop = 2*cs.DIST_THRESH, step = cs.DS)

    #     xx, yy = np.meshgrid(dist_x, dist_y)

    #     p = aux.norm([xx,yy])
    #     q = aux.norm([xx - v_beta*cs.Rt*ex, yy - v_beta*cs.Rt*ey])
    #     f = (v_beta*cs.Rt)**2
    #     b = np.sqrt((p + q)**2 - f**2)/2

    #     return np.array(cs.V_ZERO*np.exp(-b/cs.SIGMA))
