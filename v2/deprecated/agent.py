import numpy as np

from constants import *
import aux

class Agent:
    
    # Class attributes:
    
    # Initializer to define instance attributes:
    def __init__(self, id, position, desired_position):
        self.id = id # agent's unique id
        self.position = position # agent's current position
        
        self.desired_position = desired_position # agent's goal location

        self.desired_speed = np.random.normal(loc = DESIRED_SPEED_loc, scale = DESIRED_SPEED_scale) # agent's goal speed
        self.max_speed = MAX * self.desired_speed # agent's maximal speed

        desired_direction = self.calc_desired_direction()
        
        self.velocity = np.array([self.desired_speed*desired_direction[0], self.desired_speed*desired_direction[1]])
        

        self.resultant = []        
        self.track = []

        
    def calc_desired_direction(self):
        """Calculates the vector that indicates the desired direction of movement, based on the agent's 
        current position and where she wants to go."""
        ex = self.desired_position[0] - self.position[0]
        ey = self.desired_position[1] - self.position[1]
        norm_e = aux.norm([ex,ey])
        return [ex/norm_e, ey/norm_e]
    
    def V(self, ex, ey, v_beta, dt):
        '''This function gives the repulsive potential between agents.'''

        dist_x = np.arange(start = -2*DIST_THRESH, stop = 2*DIST_THRESH, step = DS)
        dist_y = np.arange(start = -2*DIST_THRESH, stop = 2*DIST_THRESH, step = DS)
        
        xx, yy = np.meshgrid(dist_x, dist_y) 

        p = aux.norm([xx,yy])
        q = aux.norm([xx - v_beta*dt*ex, yy - v_beta*dt*ey])
        f = (v_beta*dt)**2
        b = np.sqrt((p + q)**2 - f**2)/2

        return np.array(V_ZERO*np.exp(-b/SIGMA))

    def U(self):
        ''' This function gives the repulsive potential between and agent ant the surrounding borders.'''

        dist_x = np.arange(start = -2*DIST_THRESH, stop = 2*DIST_THRESH, step = DS)
        dist_y = np.arange(start = -2*DIST_THRESH, stop = 2*DIST_THRESH, step = DS)
        
        xx, yy = np.meshgrid(dist_x, dist_y) 

        dist = aux.norm([xx,yy])

        return np.array(U_ZERO*np.exp(-dist/R))

    def gradient(self, pot, x, y):

        dist_x = np.arange(start = -2*DIST_THRESH, stop = 2*DIST_THRESH, step = DS)
        dist_y = np.arange(start = -2*DIST_THRESH, stop = 2*DIST_THRESH, step = DS)

        Fx, Fy = np.gradient(pot)
        
        nx = self.find_nearest(dist_x, x)                
        ny = self.find_nearest(dist_y, y)
        
        return np.array([-Fx[nx, ny], -Fx[nx, ny]])

    def find_nearest(self, array, value):
        idx = (np.abs(array - value)).argmin()
        return idx
