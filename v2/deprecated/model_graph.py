# -*- coding: utf-8 -*-

'''
Created on 14 de abril de 2020
@author: Maria Carolina Belotti
'''

import numpy as np

import matplotlib.pyplot as plt

import pandas as pd

from datetime import datetime

from agent import Agent
from constants import *

import math

import os
import time
import networkx as nx

class Model:
    ''' We define the Model class that contains the mechanisms to generate and log a simulation.'''
    
    def __init__(self, dt):
        self.agents = [] # list of agents active in the model
        self.dt = dt # defines time step

    def create_agents(self, N):
        """Creates a set of N agents."""
        id = 0
        for i in range(math.floor(N/2)):
            desired_position = np.array([X,Y/2])
            #position = np.array([0,0])
            position = np.array([0, np.random.random()*Y])
            color = self.generate_color()
            agent = Agent(id, position, desired_position, color)
            self.agents.append(agent)
            id = id + 1
        
        for i in range(math.floor(N/2),N):
            desired_position = np.array([0,Y/2])
            #position = np.array([0,0])
            position = np.array([X, np.random.random()*Y])
            color = self.generate_color()
            agent = Agent(id, position, desired_position, color)
            self.agents.append(agent)
            id = id + 1


    def get_agents(self):
        """Returns all agents currently in the model."""
        return self.agents

    def show_state(self):
        """Displays agent's current position."""
 
        for agent in self.get_agents():
            ax.scatter(x = agent.position[0], y = agent.position[1], color = agent.color, s = 4)
            #p.circle(x = agent.position[0], y = agent.position[1], size=3, color = agent.color, alpha=0.5)


    def create_log(self, N, M):
    
        df = pd.DataFrame(columns = ['agent_id', 'step', 'position_x', 'position_y', 'velocity_x', 'velocity_y', 'resultant_x', 'resultant_y', 'time_relaxation_x', 'time_relaxation_y', 'agent_repulsion_x', 'agent_repulsion_y'])
        for agent in self.agents:
            df_ = pd.DataFrame(agent.track, columns = ['agent_id', 'step', 'position_x', 'position_y', 'velocity_x', 'velocity_y', 'resultant_x', 'resultant_y', 'time_relaxation_x', 'time_relaxation_y', 'agent_repulsion_x', 'agent_repulsion_y'])
            df = df.append(df_)
        
        now = datetime.now()

        dt_string = "sim_S"+ str(M) + "A" + str(N) + "_" + now.strftime("%H%M")
        
        folder = "./log/" + now.strftime("%m%d")
        if not os.path.exists(folder):
            os.mkdir(folder)

        filename = folder + "/" + dt_string + ".csv"

        df.to_csv(filename)
        
        return dt_string, folder
        
    def generate_color(self):
        
        r = np.random.random()
        g = np.random.random()
        b = np.random.random()
         
        return (r, g, b)


    def angle_of_view(self, e, f):

    	norm_f = np.linalg.norm(f)
    	w = np.dot(e, f)

    	if w > norm_f*np.cos(PHI):
    		return C
    	else:
    		return 1

    def graph_gen(self):

        G = nx.Graph()
        
        for agent in self.agents:
            G.add_node(agent.id)
            return G

    def step(self, i):
        '''This function evaluates movement possibilities for all agents and then moves them.'''
        print("Currently processing step " + str(i))

        G = self.graph_gen()

        for agent in self.agents:

            tot_agent_repulsion = np.array([0,0]) # f_alphabeta 
            border_repulsion = np.array([0,0]) # F_alphaB
            group_attraction = np.array([0,0]) # f_alphai                 
            time_relaxation = np.array([0,0]) # F_alphazero

            ex, ey = agent.calc_desired_direction()
                        
            for neighbor in self.agents:
                if neighbor.id != agent.id:

                    dist = agent.position - neighbor.position
                    norm_dist = np.linalg.norm(dist)

                    if norm_dist <= DIST_THRESH:

	                    v_beta = np.linalg.norm(neighbor.velocity)
	                    agent_repulsion = self.gradient(agent.V(ex, ey, v_beta, self.dt), dist[0], dist[1])        
	                    
	                    angle_of_view = self.angle_of_view(np.array([ex,ey]), agent_repulsion)
	                    agent_repulsion = agent_repulsion*angle_of_view

	                    tot_agent_repulsion = agent_repulsion + tot_agent_repulsion
	                    G.add_edge(agent.id, neighbor.id)

                    else:
                        tot_agent_repulsion = [0,0] + tot_agent_repulsion

            desired_velocity = np.array([agent.desired_speed*ex, agent.desired_speed*ey])
            time_relaxation = (desired_velocity - agent.velocity)/TAU 
            
            #rand = np.random.normal(loc = FLUCT_loc, scale = FLUCT_scale, size = 2)
            rand = [0,0]
            resultant = np.sum([time_relaxation, tot_agent_repulsion, border_repulsion, group_attraction, rand], axis = 0)
            #resultant = [1,1]
            
            self.move(agent, resultant)
            
            row = [agent.id, i, agent.position[0], agent.position[1], agent.velocity[0], agent.velocity[1], resultant[0], resultant[1], time_relaxation[0], time_relaxation[1], tot_agent_repulsion[0], tot_agent_repulsion[1]]
            agent.track.append(row)
 	          
        return G
    
    def move(self, agent, acceleration):
        '''This implements agent movement.'''

        a = acceleration
        s = agent.position
        v = agent.velocity

        new_v = v + a*self.dt
        new_s = s + new_v*self.dt
        
        new_speed = np.linalg.norm(new_v)
        
        if new_speed > agent.max_speed:
        	new_v = (new_v*agent.max_speed)/new_speed

        new_x = new_s[0]

        if new_x >= X:
            new_s[0] = X
            new_v = -new_v
            
        if new_x < 0:
            new_s[0] = 0
            new_v = -new_v
        
        new_y = new_s[1]        
        
        if new_y >= Y:
            new_s[1] = Y
            new_v = -new_v
            
        if new_y < 0:
            new_s[1] = 0
            new_v = -new_v
        
        agent.position = new_s
        agent.velocity = new_v
                    
    def gradient(self, pot, x, y):
        '''This will return the negative of the gradient of a potential (pot) for a given distance (x,y).'''
        grad = np.gradient(pot)
        grad = np.array(grad)

        Fx = - grad[0][int(x/DS)][int(y/DS)]
        Fy = - grad[1][int(x/DS)][int(y/DS)]


        return np.array([Fx, Fy])
    
    def run(self, N, M):
        self.create_agents(N)
        
        global fig 
        global ax 
        fig = plt.figure(figsize = [X/2,Y/2])
        ax = plt.axes(xlim=(0, X), ylim=(0, Y))
        ax.set_title("Diagrama de Trajetórias dos Agentes")
        
        ax.annotate('Objetivo',
            xy=(X, Y), xycoords='data',
            xytext=(-15, 25), textcoords='offset points',
            arrowprops=dict(facecolor='black', shrink=0.05),
            horizontalalignment='right', verticalalignment='bottom')

        for i in range(M):
            #print("Step: " + str(i))
            G = self.step(i)
            
            self.show_state()

            graph_name = "./log/G" + str(i) + ".adjlist"
            fh = open(graph_name, "wb")
            nx.write_adjlist(G, fh)
        
        dt_string, folder = self.create_log(N, M)

        filename = folder + "/" + dt_string +  ".png"
        
        fig.savefig(filename, dpi = 600)
        plt.show()
        print("Done!")

start = time.time()

sim = Model(dt)
sim.run(N, M)

end = time.time()
runtime = end - start
print(runtime)
