
import numpy as np

import pandas as pd
import altair as alt

from constants import *
import aux
from border import Border

import streamlit as st


sim_code = "0611/sim_S1000A10_1709"

log_path = "/home/maria/current/work/poli/masters_project/model/SocialForces/log/" + sim_code + "/tracks.csv"

@st.cache(persist = True)
def load_data():

    df = pd.read_csv(log_path)

    return df


borders = []

def create_borders():
    """ Creates borders defined in Constants.py file"""

    for index, row in BORDERS.iterrows(): 
        id = index
        
        if row["shape"] == "line":
        
            x1 = row["x1"]
            y1 = row["y1"]
            x2 = row["x2"]
            y2 = row["y2"]
            
            Qx = np.linspace(start = x1, stop = x2, num = 50)
            Qy = np.linspace(start = y1, stop = y2, num = 50)
            
        border = Border(id, Qx, Qy)
        borders.append(border)

create_borders()

border_asdf = []

for border in borders:
    Q = np.array([border.Qx, border.Qy]).T
    Q = pd.DataFrame(Q, columns = ["Qx", "Qy"])
    border_asdf.append(Q)


data = load_data()

st.title("Simulação de Comportamento Coletivo")
st.subheader("Modelo de Forças Sociais - Helbing, 1995")
st.text(sim_code)
selection = alt.selection_multi(fields=['agent_id'], bind='legend')
scales = alt.selection_interval(bind='scales')




chart11 = alt.Chart(data).mark_line().encode(
        x = alt.X('step', axis = alt.Axis(title="dt")),
        y = alt.Y('resultant_mag', axis = alt.Axis(title = "Resultante (N)")),
        color = alt.Color('agent_id:O', scale = alt.Scale(scheme = "tableau20")),
        opacity = alt.condition(selection, alt.value(1), alt.value(0.1))
    ).properties(
        height = 300,
        width = 500,
        title = "Resultante em Função do Tempo"
    ).add_selection(
    selection, scales
    )


chart12 = alt.Chart(data).mark_line().encode(
        x = alt.X('step', axis = alt.Axis(title="dt")),
        y = alt.Y('agent_repulsion_mag', axis = alt.Axis(title = "Repulsão entre Agentes (N)")),
        color = alt.Color('agent_id:O', scale = alt.Scale(scheme = "tableau20")),
        opacity = alt.condition(selection, alt.value(1), alt.value(0.1))
    ).properties(
        height = 300,
        width = 500,
        title = "Repulsão entre Agentes em Função do Tempo"
    ).add_selection(
    selection, scales
    )


chart13 = alt.Chart(data).mark_line().encode(
        x = alt.X('step', axis = alt.Axis(title="dt")),
        y = alt.Y('border_repulsion_mag', axis = alt.Axis(title = "Repulsão em Relação a Bordas (N)")),
        color = alt.Color('agent_id:O', scale = alt.Scale(scheme = "tableau20")),
        opacity = alt.condition(selection, alt.value(1), alt.value(0.1))
    ).properties(
        height = 300,
        width = 500,
        title = "Repulsão em Relação às Bordas em Função do Tempo"
    ).add_selection(
    selection, scales
    )


chart21 = alt.Chart(data).mark_line().encode(
        x = alt.X('step', axis = alt.Axis(title="dt")),
        y = alt.Y('velocity_mag', axis = alt.Axis(title = "Velocidade (m/s)")),
        color = alt.Color('agent_id:O', scale = alt.Scale(scheme = "tableau20")),
        opacity = alt.condition(selection, alt.value(1), alt.value(0.1))
    ).properties(
        height = 300,
        width = 500,
        title = "Velocidade em Função do Tempo"
    ).add_selection(
    selection, scales
    )

base = alt.Chart(data).properties(
        height = 300,
        width = 500,
        title = "Deslocamento Espacial"
    ).add_selection(
    selection, scales
    )

chart = base.mark_line().encode(
        x = alt.X('position_x', axis = alt.Axis(title="Posição x")),
        y = alt.Y('position_y', axis = alt.Axis(title = "Posição y")),
        color = alt.Color('agent_id:O', scale = alt.Scale(scheme = "tableau20")),
        opacity = alt.condition(selection, alt.value(1), alt.value(0.1)),
        tooltip = "step"
    )


for border in border_asdf:

    line = alt.Chart(border).mark_line().encode(
        x = "Qx",
        y = "Qy",
        color = alt.value("#070707"),
        )
    chart = chart + line
    
 

chart23 = alt.Chart(data).mark_line().encode(
        x = alt.X('step', axis = alt.Axis(title="dt")),
        y = alt.Y('time_relaxation_mag', axis = alt.Axis(title = "Força de Relaxamento (N)")),
        color = alt.Color('agent_id:O', scale = alt.Scale(scheme = "tableau20")),
        opacity = alt.condition(selection, alt.value(1), alt.value(0.1))
    ).properties(
        height = 300,
        width = 500,
        title = "Força de Relaxamento em Função do Tempo"
    ).add_selection(
    selection, scales
    )

chart1 = alt.hconcat(chart11, chart12, chart13, spacing = 100)
chart2 = alt.hconcat(chart21,chart, chart23, spacing = 100)

chart = alt.vconcat(chart1, chart2, spacing = 70, padding = {"left": 40, "top": 40, "right": 40, "bottom": 40})


st.altair_chart(chart)



