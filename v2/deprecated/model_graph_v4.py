# -*- coding: utf-8 -*-

'''
Created on 14 de abril de 2020
@author: Maria Carolina Belotti
'''

import numpy as np

import matplotlib.pyplot as plt

import pandas as pd

from datetime import datetime

from agent import Agent
from constants import *
from border import Border
import aux

import viz

import math

import os
import time
import networkx as nx

class Model:
    ''' We define the Model class that contains the mechanisms to generate and log a simulation.'''
    
    def __init__(self, dt):
        self.agents = [] # list of agents active in the model
        self.dt = dt # defines time step
        self.borders = [] # list of borders defined in the model

    def create_agents(self, N):
        """Creates a set of N agents."""
        id = 0
        for i in range(int(N)):
            desired_position = [X, np.random.random()*Y]
            #position = np.array([0,0])
            position = np.array([0, np.random.random()*Y])
            # color = self.generate_color()
            agent = Agent(id, position, desired_position)
            self.agents.append(agent)
            id = id + 1

        # for i in range(int(N/2), N):
        #     desired_position = [0, np.random.random()*Y]
        #     #position = np.array([0,0])
        #     position = np.array([X, np.random.random()*Y])
        #     # color = self.generate_color()
        #     agent = Agent(id, position, desired_position)
        #     self.agents.append(agent)
        #     id = id + 1

    def create_borders(self):
        """ Creates borders defined in Constants.py file"""

        for index, row in BORDERS.iterrows(): 
            id = index
            
            if row["shape"] == "line":
            
                x1 = row["x1"]
                y1 = row["y1"]
                x2 = row["x2"]
                y2 = row["y2"]
                
                Qx = np.linspace(start = x1, stop = x2, num = 50)
                Qy = np.linspace(start = y1, stop = y2, num = 50)

            border = Border(id, Qx, Qy)
            self.borders.append(border)

    def get_borders(self):
        """ Returns all borders currently in the model."""
        return self.borders


    def get_agents(self):
        """Returns all agents currently in the model."""
        return self.agents

    def create_log(self, N, M):
        '''Creates a dataframe and a log file with the data.'''
    
        df = []

        for agent in self.agents:
            for row in agent.track:
                df.append(row)

        df = pd.DataFrame(df, columns = ['agent_id', 'step', 'position_x', 'position_y', 'velocity_x', 'velocity_y', 'resultant_x', 'resultant_y', 'time_relaxation_x', 'time_relaxation_y', 'agent_repulsion_x', 'agent_repulsion_y', "border_repulsion_x", "border_repulsion_y"])
            
        resultant_mag = []
        agent_repulsion_mag = []
        velocity_mag = []
        time_relaxation_mag = []
        border_repulsion_mag = []

        for i in range(len(df)):
            resultant = np.array([df["resultant_x"][i], df["resultant_y"][i]])
            resultant_mag.append(aux.norm(resultant))

            agent_repulsion = np.array([df["agent_repulsion_x"][i], df["agent_repulsion_y"][i]])
            agent_repulsion_mag.append(aux.norm(agent_repulsion))

            velocity = np.array([df["velocity_x"][i], df["velocity_y"][i]])
            velocity_mag.append(aux.norm(velocity))

            time_relaxation = np.array([df["time_relaxation_x"][i], df["time_relaxation_y"][i]])
            time_relaxation_mag.append(aux.norm(time_relaxation))

            border_repulsion = np.array([df["border_repulsion_x"][i], df["border_repulsion_y"][i]])
            border_repulsion_mag.append(aux.norm(border_repulsion))

            
        df["resultant_mag"] = resultant_mag
        df["velocity_mag"] = velocity_mag
        df["agent_repulsion_mag"] = agent_repulsion_mag
        df["time_relaxation_mag"] = time_relaxation_mag
        df["border_repulsion_mag"] = border_repulsion_mag

        return df

    def angle_of_view(self, e, f):

        norm_f = aux.norm(f)
        w = np.dot(e, f)

        if w > norm_f*np.cos(PHI):
            return C
        else:
            return 1

    def graph_gen(self):

        G = nx.Graph()
        
        for agent in self.agents:
            G.add_node(agent.id)
            return G

    def step(self, i):
        '''This function evaluates movement possibilities for all agents and then moves them.'''

        G = self.graph_gen()

        np.random.shuffle(self.agents)
        for agent in self.agents:

            tot_agent_repulsion = np.array([0,0]) # f_alphabeta 
            tot_border_repulsion = np.array([0,0]) # F_alphaB
            group_attraction = np.array([0,0]) # f_alphai                 
            time_relaxation = np.array([0,0]) # F_alphazero

            ex, ey = agent.calc_desired_direction()
                        
            for neighbor in self.agents:
                if neighbor.id != agent.id:

                    dist = agent.position - neighbor.position
                    norm_dist = np.linalg.norm(dist)

                    if norm_dist <= DIST_THRESH:

                        v_beta = aux.norm(neighbor.velocity)
                        ex_beta, ey_beta = neighbor.calc_desired_direction()

                        agent_repulsion = agent.gradient(agent.V(ex_beta, ey_beta, v_beta, self.dt), dist[0], dist[1])        

                        angle_of_view = self.angle_of_view(np.array([ex,ey]), agent_repulsion)
                        agent_repulsion = agent_repulsion*angle_of_view

                        tot_agent_repulsion = agent_repulsion + tot_agent_repulsion
                        G.add_edge(agent.id, neighbor.id)

                    else:
                        tot_agent_repulsion = [0,0] + tot_agent_repulsion


            for border in self.borders:

                border_dist_min = border.calc_dist(agent.position[0], agent.position[1], border.id)  
                dist_norm = aux.norm(border_dist_min)

                if dist_norm <= DIST_THRESH:
                    border_repulsion = agent.gradient(agent.U(), border_dist_min[0], border_dist_min[1])
                    
                    angle_of_view = self.angle_of_view(np.array([ex,ey]), border_repulsion)
                    border_repulsion = border_repulsion*angle_of_view

                else:
                    border_repulsion = [0,0]

                tot_border_repulsion = tot_border_repulsion + border_repulsion
            


            desired_velocity = np.array([agent.desired_speed*ex, agent.desired_speed*ey])
            time_relaxation = (desired_velocity - agent.velocity)/TAU 
            
            #rand = np.random.normal(loc = FLUCT_loc, scale = FLUCT_scale, size = 2)
            rand = [0,0]
            resultant = np.sum([time_relaxation, tot_agent_repulsion, tot_border_repulsion, group_attraction, rand], axis = 0)
            #resultant = [1,1]
            
            self.move(agent, resultant)
            
            row = [agent.id, i, agent.position[0], agent.position[1], agent.velocity[0], agent.velocity[1], resultant[0], resultant[1], time_relaxation[0], time_relaxation[1], tot_agent_repulsion[0], tot_agent_repulsion[1], tot_border_repulsion[0], tot_border_repulsion[1]]
            agent.track.append(row)
              
        return G
    
    def move(self, agent, acceleration):
        '''This implements agent movement.'''

        a = acceleration
        s = agent.position
        v = agent.velocity

        new_v = v + a*self.dt
        new_s = s + new_v*self.dt
        
        new_speed = np.linalg.norm(new_v)
        
        if new_speed > agent.max_speed:
            new_v = (new_v*agent.max_speed)/new_speed

        new_x = new_s[0]

        if new_x >= X:
            new_s[0] = X
            new_v = -new_v
            
        if new_x < 0:
            new_s[0] = 0
            new_v = -new_v
        
        new_y = new_s[1]        
        
        if new_y >= Y:
            new_s[1] = Y
            new_v = -new_v
            
        if new_y < 0:
            new_s[1] = 0
            new_v = -new_v
        
        agent.position = new_s
        agent.velocity = new_v
                    
    def run(self, N, M):
        ''' The the model for N agents and M time steps. '''
        
        self.create_agents(N)
        self.create_borders()

        now = datetime.now()

        dt_string = "sim_S"+ str(M) + "A" + str(N) + "_" + now.strftime("%H%M")
        
        folder = "./log/" + now.strftime("%m%d")

        if not os.path.exists(folder):
            os.mkdir(folder)

        log_path = folder + "/" + dt_string + "/"

        if not os.path.exists(log_path):
            os.mkdir(log_path)


        print("Log will be created in " + log_path)

        for i in range(M):
            print("Step: " + str(i))
            
            G = self.step(i)

            if not os.path.exists(log_path + "graphs/"):
                os.mkdir(log_path + "graphs/")
            
            graph_name = log_path + "graphs/G" + str(i) + ".adjlist"
            
            fh = open(graph_name, "wb")
            nx.write_adjlist(G, fh)
        
        df  = self.create_log(N, M)

        df.to_csv(log_path + "tracks.csv")

        os.system('cp constants.py ' + log_path + 'constants.py')  

        viz.get_viz(log_path, self.borders)

        print("Done!")

start = time.time()

sim = Model(dt)
sim.run(N, M)

end = time.time()
runtime = end - start
print(runtime)
