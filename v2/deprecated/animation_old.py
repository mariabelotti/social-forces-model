from matplotlib.animation import FuncAnimation

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib 

from constants import *
from border import Border


cmap = matplotlib.cm.get_cmap("tab20c")

sim_code = "0828/sim_S1000A100_V_ZERO2.1sc4"

log_path = "/home/maria/current/work/poli/masters_project/model/SocialForces/v2/log/" + sim_code + "/"
log_file = log_path + "tracks.csv"

df = pd.read_csv(log_file)
df = df[df.step != 0]

def initialize_borders():
    
    borders = []

    for index, row in BORDERS.iterrows(): 
        uid = index
                
        if row["shape"] == "line":
                
            x1 = row["x1"]
            y1 = row["y1"]
            x2 = row["x2"]
            y2 = row["y2"]
                    
            Qx = np.linspace(start=x1, stop=x2, num=50)
            Qy = np.linspace(start=y1, stop=y2, num=50)

        borders.append(Border(uid, Qx, Qy))

    return borders

def plot_borders(borders):

    for border in borders:
        Q = np.array([border.Qx, border.Qy]).T
        ax.plot(Q[:,0], Q[:,1], linewidth = 2, linestyle = '-', color = "black")


class dot():
    def __init__(self, i):
            
        self.subset = df[df["agent_id"] == i]
                
        first_step = self.subset[self.subset["step"] == 1]
        self.x = first_step["position_x"]
        self.y = first_step["position_y"]
        self.speed = float(first_step["velocity_mag"].values)
        self.color = cmap(i/N)
        
    def move(self, f):
        step = self.subset[self.subset["step"] == f+1]
        new_pos = step.iloc[:,2:4].values
        self.x = new_pos[0,0]
        self.y = new_pos[0,1]
        self.speed = float(step["velocity_mag"].values)
            
           
dots = []

i = 0
for i in range(N):
    dots.append(dot(i))
    i = i + 1    

borders = initialize_borders()

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()

ax = plt.axes(xlim=(0, X), ylim=(0, Y))
ax.set_xlim(0,X)
ax.set_ylim(0,Y)

for dot in dots:
    d = ax.scatter(dot.x, dot.y, color = 'white', s = 5)

plot_borders(borders)

    
def animate(f):
    speeds = []
    positions = []
    colors = []
    for dot in dots:
        # we will move the dot to the next step:
        dot.move(f)
        positions.append([dot.x, dot.y])
        colors.append(dot.color)
        speeds.append(dot.speed*80)
        
    positions = np.array(positions)

    d.set_offsets(positions)
    d.set_edgecolors(colors)
    d.set_facecolors(colors)
    d.set_sizes(speeds)

    
# call the animator
anim = FuncAnimation(fig, animate, frames=M, interval= 10)

filename = log_path + "animate.mp4"
anim.save(filename, writer='ffmpeg', dpi=300)

