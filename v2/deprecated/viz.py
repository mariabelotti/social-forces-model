# -*- coding: utf-8 -*-

'''
Created on 14 de abril de 2020
@author: Maria Carolina Belotti
'''

import numpy as np

import plotly.graph_objects as go
import plotly.express as px

import matplotlib.pyplot as plt

import os

import pandas as pd

input_path = "/home/maria/current/work/poli/masters_project/model/SocialForces/log/0430/sim_S1000A50_2025.csv"

output_path = input_path[:-4] + "/"

if not os.path.exists(output_path):
	os.mkdir(output_path)


df = pd.read_csv(input_path)

X = 50
Y = 20


resultant_mag = []
agent_repulsion_mag = []
velocity_mag = []
time_relaxation_mag = []

for i in range(len(df)):
    resultant = [df["resultant_x"][i], df["resultant_y"][i]]
    resultant_mag.append(np.linalg.norm(resultant))

    agent_repulsion = [df["agent_repulsion_x"][i], df["agent_repulsion_y"][i]]
    agent_repulsion_mag.append(np.linalg.norm(agent_repulsion))

    velocity = [df["velocity_x"][i], df["velocity_y"][i]]
    velocity_mag.append(np.linalg.norm(velocity))

    time_relaxation = [df["time_relaxation_x"][i], df["time_relaxation_y"][i]]
    time_relaxation_mag.append(np.linalg.norm(time_relaxation))

    
df["resultant_mag"] = resultant_mag
df["velocity_mag"] = velocity_mag
df["agent_repulsion_mag"] = agent_repulsion_mag
df["time_relaxation_mag"] = time_relaxation_mag

agent_list = df.agent_id.unique()


fig, axs = plt.subplots(2, 2, figsize = [15,10])


axs[0,0].set_title("Time Relaxation vs. Time Step")
    
for agent_id in agent_list: 

    subset = df[df["agent_id"] == agent_id]
    
    axs[0,0].plot(subset["step"], subset["time_relaxation_mag"])
    axs[0,0].set_xlabel("Time Step (dt)")
    axs[0,0].set_ylabel("Time Relaxation")
    

axs[0,1].set_title("Agent Repulsion vs. Time Step")

for agent_id in agent_list: 

    subset = df[df["agent_id"] == agent_id]
    
    axs[0,1].plot(subset["step"], subset["agent_repulsion_mag"])
    axs[0,1].set_xlabel("Time Step (dt)")
    axs[0,1].set_ylabel("Agent Repulsion")
    


axs[1,0].set_title("Resultant vs. Time Step")

for agent_id in agent_list: 

    subset = df[df["agent_id"] == agent_id]
    
    axs[1,0].plot(subset["step"], subset["resultant_mag"])
    axs[1,0].set_xlabel("Time Step (dt)")
    axs[1,0].set_ylabel("Resultant")
    


axs[1,1].set_title("Velocity vs. Time Step")

for agent_id in agent_list: 

    subset = df[df["agent_id"] == agent_id]
    
    axs[1,1].plot(subset["step"], subset["velocity_mag"])
    axs[1,1].set_xlabel("Time Step (dt)")
    axs[1,1].set_ylabel("Velocity")
    
fig.savefig(output_path + "grid.png", dpi = 600, bbox_inches = 'tight')