# -*- coding: utf-8 -*-

'''
Created on 14 de abril de 2020
@author: Maria Carolina Belotti
'''

import numpy as np

import plotly.graph_objects as go
import plotly.express as px

import matplotlib.pyplot as plt

import os

import pandas as pd

	
input_path = "/home/maria/current/work/poli/masters_project/model/SocialForces/log/0504/sim_S10A1_0943.csv"

output_path = input_path[:-4] + "/"

if not os.path.exists(output_path):
	os.mkdir(output_path)


df = pd.read_csv(input_path)


resultant_mag = []
agent_repulsion_mag = []
velocity_mag = []
time_relaxation_mag = []
border_repulsion_mag = []

for i in range(len(df)):
    resultant = [df["resultant_x"][i], df["resultant_y"][i]]
    resultant_mag.append(np.linalg.norm(resultant))

    agent_repulsion = [df["agent_repulsion_x"][i], df["agent_repulsion_y"][i]]
    agent_repulsion_mag.append(np.linalg.norm(agent_repulsion))

    velocity = [df["velocity_x"][i], df["velocity_y"][i]]
    velocity_mag.append(np.linalg.norm(velocity))

    time_relaxation = [df["time_relaxation_x"][i], df["time_relaxation_y"][i]]
    time_relaxation_mag.append(np.linalg.norm(time_relaxation))

    border_repulsion = [df["border_repulsion_x"][i], df["border_repulsion_y"][i]]
    border_repulsion_mag.append(np.linalg.norm(border_repulsion))

    
df["resultant_mag"] = resultant_mag
df["velocity_mag"] = velocity_mag
df["agent_repulsion_mag"] = agent_repulsion_mag
df["time_relaxation_mag"] = time_relaxation_mag
df["border_repulsion_mag"] = border_repulsion_mag

agent_list = df.agent_id.unique()


fig, axs = plt.subplots(3, 3, figsize = [15,10])

    
for agent_id in agent_list: 

    subset = df[df["agent_id"] == agent_id]
    
    axs[0,0].set_title("Time Relaxation vs. Time Step")

    axs[0,0].plot(subset["step"], subset["time_relaxation_mag"])
    axs[0,0].set_xlabel("Time Step (dt)")
    axs[0,0].set_ylabel("Time Relaxation")

    axs[0,1].set.title("Agent Repulsion vs. Time Step")

    axs[0,1].plot(subset["step"], subset["agent_repulsion_mag"])
    axs[0,1].set_xlabel("Time Step (dt)")
    axs[0,1].set_ylabel("Agent Repulsion")
    

    axs[1,0].set_title("Resultant vs. Time Step")
    
    axs[1,0].plot(subset["step"], subset["resultant_mag"])
    axs[1,0].set_xlabel("Time Step (dt)")
    axs[1,0].set_ylabel("Resultant")

    
    axs[1,1].set_title("Velocity vs. Time Step")

    axs[1,1].plot(subset["step"], subset["velocity_mag"])
    axs[1,1].set_xlabel("Time Step (dt)")
    axs[1,1].set_ylabel("Velocity")


    axs[2,1].set_title("Trajectories") 

    axs[2,1].plot(subset["position_x"], subset["position_y"])
    axs[2,1].set_xlabel("Position x")
    axs[2,1].set_ylabel("Position y")

    axs[2,2].set_title("Border Repulsion")
    
    axs[2,2].plot(subset["step"], subset["border_repulsion_mag"])
    axs[2,2].set_xlabel("Step (dt)")
    axs[2,2].set_ylabel("Border Repulsion")
    

fig.savefig(output_path + "grid.png", dpi = 600)
