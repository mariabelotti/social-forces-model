# -*- coding: utf-8 -*-

'''
Created on 14 de abril de 2020
@author: Maria Carolina Belotti
'''

import numpy as np

import matplotlib.pyplot as plt

import pandas as pd

from datetime import datetime

from agent import Agent
from constants import *
from border import Border

import math

import os
import time
import networkx as nx

class Model:
    ''' We define the Model class that contains the mechanisms to generate and log a simulation.'''
    
    def __init__(self, dt):
        self.agents = [] # list of agents active in the model
        self.dt = dt # defines time step
        self.borders = [] # list of borders defined in the model

    def create_agents(self, N):
        """Creates a set of N agents."""
        id = 0
        for i in range(N):
            desired_position = DESIRED_POSITION
            #position = np.array([0,0])
            position = np.array([0, np.random.random()*Y])
            color = self.generate_color()
            agent = Agent(id, position, desired_position, color)
            self.agents.append(agent)
            id = id + 1

    def create_borders(self):
        """ Creates borders defined in Constants.py file"""
        id = 0

        for i in range(len(A)):
            id = i
            print("Border: " + str(id))
            border = Border(id, A[i], B[i], C[i])
            self.borders.append(border)

    def get_borders(self):
        """ Returns all borders currently in the model."""
        return self.borders


    def get_agents(self):
        """Returns all agents currently in the model."""
        return self.agents

    # def show_state(self):
    #     """Displays agent's current position."""
 
    #     for agent in self.get_agents():
    #         ax.scatter(x = agent.position[0], y = agent.position[1], color = agent.color, s = 4)
            #p.circle(x = agent.position[0], y = agent.position[1], size=3, color = agent.color, alpha=0.5)


        # for border in self.get_borders():
        #   x, y = border.plot_border()
        #   ax.plot(x, y, color = "black", linewidth = 10)




    def create_log(self, N, M):
    
        df = pd.DataFrame(columns = ['agent_id', 'step', 'position_x', 'position_y', 'velocity_x', 'velocity_y', 'resultant_x', 'resultant_y', 'time_relaxation_x', 'time_relaxation_y', 'agent_repulsion_x', 'agent_repulsion_y', "border_repulsion_x", "border_repulsion_y"])
        for agent in self.agents:
            df_ = pd.DataFrame(agent.track, columns = ['agent_id', 'step', 'position_x', 'position_y', 'velocity_x', 'velocity_y', 'resultant_x', 'resultant_y', 'time_relaxation_x', 'time_relaxation_y', 'agent_repulsion_x', 'agent_repulsion_y', "border_repulsion_x", "border_repulsion_y"])
            df = df.append(df_)
        
        now = datetime.now()

        dt_string = "sim_S"+ str(M) + "A" + str(N) + "_" + now.strftime("%H%M")
        
        folder = "./log/" + now.strftime("%m%d")
        if not os.path.exists(folder):
            os.mkdir(folder)

        log_path = folder + "/" + dt_string

        
        df.to_csv(log_path + ".csv")
        self.viz(log_path)

        print("Log created in " + log_path)
        
    def generate_color(self):
        
        r = np.random.random()
        g = np.random.random()
        b = np.random.random()
         
        return (r, g, b)


    def angle_of_view(self, e, f):

        norm_f = np.linalg.norm(f)
        w = np.dot(e, f)

        if w > norm_f*np.cos(PHI):
            return C
        else:
            return 1

    def graph_gen(self):

        G = nx.Graph()
        
        for agent in self.agents:
            G.add_node(agent.id)
            return G

    def step(self, i):
        '''This function evaluates movement possibilities for all agents and then moves them.'''
        print("Currently processing step " + str(i))

        G = self.graph_gen()

        for agent in self.agents:

            tot_agent_repulsion = np.array([0,0]) # f_alphabeta 
            tot_border_repulsion = np.array([0,0]) # F_alphaB
            group_attraction = np.array([0,0]) # f_alphai                 
            time_relaxation = np.array([0,0]) # F_alphazero

            ex, ey = agent.calc_desired_direction()
                        
            for neighbor in self.agents:
                if neighbor.id != agent.id:

                    dist = agent.position - neighbor.position
                    norm_dist = np.linalg.norm(dist)

                    if norm_dist <= DIST_THRESH:

                        v_beta = np.linalg.norm(neighbor.velocity)

                        agent_repulsion = self.gradient(agent.V(ex, ey, v_beta, self.dt), dist[0], dist[1])        

                        angle_of_view = self.angle_of_view(np.array([ex,ey]), agent_repulsion)
                        agent_repulsion = agent_repulsion*angle_of_view

                        tot_agent_repulsion = agent_repulsion + tot_agent_repulsion
                        G.add_edge(agent.id, neighbor.id)

                    else:
                        tot_agent_repulsion = [0,0] + tot_agent_repulsion


            for border in self.borders:

                border_dist_min = border.calc_dist(agent.position[0], agent.position[1], border.id)  
                dist_norm = np.linalg.norm(border_dist_min)
                if dist_norm <= DIST_THRESH:
                    border_repulsion = self.gradient(agent.U(), border_dist_min[0], border_dist_min[1])
                else:
                    border_repulsion = [0,0]

                tot_border_repulsion = tot_border_repulsion + border_repulsion
            


            desired_velocity = np.array([agent.desired_speed*ex, agent.desired_speed*ey])
            time_relaxation = (desired_velocity - agent.velocity)/TAU 
            
            #rand = np.random.normal(loc = FLUCT_loc, scale = FLUCT_scale, size = 2)
            rand = [0,0]
            resultant = np.sum([time_relaxation, tot_agent_repulsion, tot_border_repulsion, group_attraction, rand], axis = 0)
            #resultant = [1,1]
            
            self.move(agent, resultant)
            
            row = [agent.id, i, agent.position[0], agent.position[1], agent.velocity[0], agent.velocity[1], resultant[0], resultant[1], time_relaxation[0], time_relaxation[1], tot_agent_repulsion[0], tot_agent_repulsion[1], tot_border_repulsion[0], tot_border_repulsion[1]]
            agent.track.append(row)
              
        return G
    
    def move(self, agent, acceleration):
        '''This implements agent movement.'''

        a = acceleration
        s = agent.position
        v = agent.velocity

        new_v = v + a*self.dt
        new_s = s + new_v*self.dt
        
        new_speed = np.linalg.norm(new_v)
        
        if new_speed > agent.max_speed:
            new_v = (new_v*agent.max_speed)/new_speed

        new_x = new_s[0]

        if new_x >= X:
            new_s[0] = X
            new_v = -new_v
            
        if new_x < 0:
            new_s[0] = 0
            new_v = -new_v
        
        new_y = new_s[1]        
        
        if new_y >= Y:
            new_s[1] = Y
            new_v = -new_v
            
        if new_y < 0:
            new_s[1] = 0
            new_v = -new_v
        
        agent.position = new_s
        agent.velocity = new_v
                    
    def gradient(self, pot, x, y):
        '''This will return the negative of the gradient of a potential (pot) for a given distance (x,y).'''
        grad = np.gradient(pot)
        grad = np.array(grad)

        Fx = - grad[0][int(x/DS)][int(y/DS)]
        Fy = - grad[1][int(x/DS)][int(y/DS)]


        return np.array([Fx, Fy])


    def viz(self, filename):
        df = pd.read_csv(filename + ".csv")

        resultant_mag = []
        agent_repulsion_mag = []
        velocity_mag = []
        time_relaxation_mag = []
        border_repulsion_mag = []

        print(len(df))
        for i in range(len(df)):
            resultant = [df["resultant_x"][i], df["resultant_y"][i]]
            resultant_mag.append(np.linalg.norm(resultant))

            agent_repulsion = [df["agent_repulsion_x"][i], df["agent_repulsion_y"][i]]
            agent_repulsion_mag.append(np.linalg.norm(agent_repulsion))

            velocity = [df["velocity_x"][i], df["velocity_y"][i]]
            velocity_mag.append(np.linalg.norm(velocity))

            time_relaxation = [df["time_relaxation_x"][i], df["time_relaxation_y"][i]]
            time_relaxation_mag.append(np.linalg.norm(time_relaxation))

            border_repulsion = [df["border_repulsion_x"][i], df["border_repulsion_y"][i]]
            border_repulsion_mag.append(np.linalg.norm(border_repulsion))

            
        df["resultant_mag"] = resultant_mag
        df["velocity_mag"] = velocity_mag
        df["agent_repulsion_mag"] = agent_repulsion_mag
        df["time_relaxation_mag"] = time_relaxation_mag
        df["border_repulsion_mag"] = border_repulsion_mag

        agent_list = df.agent_id.unique()


        fig, axs = plt.subplots(3, 2, figsize = [15,10])

            
        for agent_id in agent_list: 

            subset = df[df["agent_id"] == agent_id]
            
            axs[0,0].set_title("Time Relaxation Force vs. Time Step")

            axs[0,0].plot(subset["step"], subset["time_relaxation_mag"])
            axs[0,0].set_xlabel("Time Step (dt)")
            axs[0,0].set_ylabel("Time Relaxation")

            axs[0,1].set_title("Agent Repulsion vs. Time Step")

            axs[0,1].plot(subset["step"], subset["agent_repulsion_mag"])
            axs[0,1].set_xlabel("Time Step (dt)")
            axs[0,1].set_ylabel("Agent Repulsion")
           

            axs[1,0].set_title("Resultant vs. Time Step")
            
            axs[1,0].plot(subset["step"], subset["resultant_mag"])
            axs[1,0].set_xlabel("Time Step (dt)")
            axs[1,0].set_ylabel("Resultant")

            
            axs[1,1].set_title("Velocity vs. Time Step")

            axs[1,1].plot(subset["step"], subset["velocity_mag"])
            axs[1,1].set_xlabel("Time Step (dt)")
            axs[1,1].set_ylabel("Velocity")


            axs[2,0].set_title("Trajectories") 

            axs[2,0].scatter(subset["position_x"], subset["position_y"], s = 1)
            axs[2,0].set_xlabel("Position x")
            axs[2,0].set_ylabel("Position y")
            axs[2,0].hlines(DIST_THRESH, xmin = 0, xmax = X, linestyles = "dashed")

            for border in self.borders:
                Q = border.get_border(border.id)
                axs[2,0].plot(Q[:,0], Q[:,1], linewidth = 1, color = "black")


            axs[2,1].set_title("Border Repulsion")
            
            axs[2,1].plot(subset["step"], subset["border_repulsion_mag"])
            axs[2,1].set_xlabel("Step (dt)")
            axs[2,1].set_ylabel("Border Repulsion")


        
        plt.tight_layout()

        fig.savefig(filename + ".png", dpi = 600)


    def run(self, N, M):
        self.create_agents(N)
        self.create_borders()

        # global fig 
        # global ax 
        # fig = plt.figure(figsize = [X/2,Y/2])
        # ax = plt.axes(xlim=(0, X), ylim=(0, Y))
        # ax.set_title("Diagrama de Trajetórias dos Agentes")
        
        # ax.annotate('Objetivo',
        #     xy=(X, Y), xycoords='data',
        #     xytext=(-15, 25), textcoords='offset points',
        #     arrowprops=dict(facecolor='black', shrink=0.05),
        #     horizontalalignment='right', verticalalignment='bottom')

        for i in range(M):
            #print("Step: " + str(i))
            G = self.step(i)
            
            #self.show_state()

            graph_name = "./graphs/G" + str(i) + ".adjlist"
            fh = open(graph_name, "wb")
            nx.write_adjlist(G, fh)
        
        self.create_log(N, M)

        # filename = folder + "/" + dt_string +  ".png"
        
        # fig.savefig(filename, dpi = 600)
        # plt.show()
        print("Done!")

start = time.time()

sim = Model(dt)
sim.run(N, M)

end = time.time()
runtime = end - start
print(runtime)
