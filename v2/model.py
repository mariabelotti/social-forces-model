# -*- coding: utf-8 -*-

'''
Created on 14 de abril de 2020
@author: Maria Carolina Belotti
'''

from datetime import datetime
import os
import time


import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd
import csv

from agent import Agent
import constants as cs
from border import Border
import aux_func as aux
import viz


now = datetime.now() # keep track of execution time

class Model:
    ''' We define the Model class that contains the mechanisms to generate and log a simulation.'''

    def __init__(self, scenario):
        self.agents = [] # list of agents active in the model
        self.borders = [] # list of borders defined in the model
        self.scenario = scenario

    def create_agents(self):

        ## Scenario #1: Agents against border:

        if self.scenario == "1":

            Y_pos = np.linspace(start = 0, stop = 10, num = cs.N)

            cs.BORDERS = pd.read_csv("./borders.csv")

            for uid in range(cs.N):
                position = np.array([0, Y_pos[uid]])
                desired_direction = np.array([1, 0])
                from_zero = False
                adapt = True
                turn = False
                agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                self.agents.append(agent)


        ## Scenario #2: Two agents against each other:

        elif self.scenario == "2":

            cs.BORDERS = pd.read_csv("./borders_simple.csv")

            from_zero = False
            adapt = False
            turn = False

            uid = 0
            desired_direction = np.array([0, -1])
            position = np.array([5,8])
            agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
            self.agents.append(agent)

            uid = 1
            desired_direction = np.array([0,1])
            position = np.array([5, 2])
            agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
            self.agents.append(agent)


        ## Scenario #3: Two groups of agents against each other:

        elif self.scenario == "3":

            cs.BORDERS = pd.read_csv("./borders_simple.csv")

            from_zero = False
            adapt = False
            turn = False

            p = int(cs.N/2)

            Y_pos = np.linspace(start = 0, stop = 10, num = p)

            for uid in range(int(cs.N/2)):

                desired_direction = np.array([1,0])
                position = np.array([0, Y_pos[uid]])
                #position = np.array([0, 5])
                agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                self.agents.append(agent)
                uid = uid + 1

            for uid in range(p, cs.N):

                desired_direction = np.array([-1,0])
                position = np.array([10, Y_pos[uid-p]])
                #position = np.array([0, 5])
                agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                self.agents.append(agent)
                uid = uid + 1


        ## Scenario #4: Agents in complex grid of squares:

        elif self.scenario == "4":

            cs.BORDERS = pd.read_csv("./borders_grid.csv")

            from_zero = False
            adapt = True
            turn = False

            X_pos = np.linspace(start=400, stop=600, num=int(np.floor(cs.N/4)))
            Y_pos = np.linspace(start=321, stop=328, num = 4)

            uid = 0

            for x in X_pos:
                for y in Y_pos:

                    position = np.array([x,y])
                    desired_direction = np.array([1,0])

                    agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                    self.agents.append(agent)
                    uid = uid + 1

        ## Scenario #5: Agents in a long corridor:

        elif self.scenario == "5A":

            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False

            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0

            for x in X_pos1:
                for y in Y_pos1:

                    position = np.array([x,y])
                    desired_direction = np.array([1,0])

                    agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                    self.agents.append(agent)
                    uid = uid + 1

        elif self.scenario == "5B":

            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False

            occupation = np.ones([9,6])

            occupation[0,3] = 0
            occupation[1,2] = 0
            occupation[2,5] = 0
            occupation[3,0] = 0
            occupation[4,4] = 0
            occupation[5,3] = 0
            occupation[6,1] = 0
            occupation[7,4] = 0
            occupation[8,3] = 0

            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:

                        position = np.array([X_pos1[i],Y_pos1[j]])
                        desired_direction = np.array([1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

            occupation = np.zeros([9,6])

            occupation[0,5] = 1
            occupation[1,1] = 1
            occupation[2,0] = 1
            occupation[3,4] = 1
            occupation[4,2] = 1
            occupation[5,2] = 1
            occupation[6,5] = 1
            occupation[7,3] = 1
            occupation[8,1] = 1

            X_pos2 = np.linspace(start=26, stop=38, num=9)
            Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:

                        position = np.array([X_pos2[i],Y_pos2[j]])
                        desired_direction = np.array([-1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

        elif self.scenario == "5C":

            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False

            occupation = np.ones([9,6])

            occupation[0,[3,2]] = 0
            occupation[1,[2,4]] = 0
            occupation[2,[5,4]] = 0
            occupation[3,[2,3]] = 0
            occupation[4,[2,4]] = 0
            occupation[5,[1,5]] = 0
            occupation[6,[2,4]] = 0
            occupation[7,[2,5]] = 0
            occupation[8,[1,4]] = 0

            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:

                        position = np.array([X_pos1[i],Y_pos1[j]])
                        desired_direction = np.array([1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

            occupation = np.zeros([9,6])

            occupation[0,[2,5]] = 1
            occupation[1,[0,3]] = 1
            occupation[2,[1,4]] = 1
            occupation[3,[4,5]] = 1
            occupation[4,[0,3]] = 1
            occupation[5,[1,2]] = 1
            occupation[6,[1,5]] = 1
            occupation[7,[0,3]] = 1
            occupation[8,[1,4]] = 1

            X_pos2 = np.linspace(start=26, stop=38, num=9)
            Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:

                        position = np.array([X_pos2[i],Y_pos2[j]])
                        desired_direction = np.array([-1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

        elif self.scenario == "5D":

            cs.BORDERS = pd.read_csv("./borders_long.csv")

            from_zero = False
            adapt = False
            turn = False

            occupation = np.zeros([9,6])

            occupation[0,[0,2,5]] = 1
            occupation[1,[0,1,4]] = 1
            occupation[2,[2,4,5]] = 1
            occupation[3,[0,1,3]] = 1
            occupation[4,[0,3,5]] = 1
            occupation[5,[1,3,5]] = 1
            occupation[6,[0,1,4]] = 1
            occupation[7,[0,2,5]] = 1
            occupation[8,[1,4,5]] = 1

            X_pos1 = np.linspace(start=0, stop=12, num=9)
            Y_pos1 = np.linspace(start=0.25, stop=2.75, num=6)

            uid = 0

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:

                        position = np.array([X_pos1[i],Y_pos1[j]])
                        desired_direction = np.array([1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1

            occupation = np.zeros([9,6])

            occupation[0,[0,1,4]] = 1
            occupation[1,[0,3,5]] = 1
            occupation[2,[0,1,4]] = 1
            occupation[3,[1,2,5]] = 1
            occupation[4,[2,4,5]] = 1
            occupation[5,[0,3,5]] = 1
            occupation[6,[0,2,4]] = 1
            occupation[7,[3,4,5]] = 1
            occupation[8,[0,4,5]] = 1

            X_pos2 = np.linspace(start=26, stop=38, num=9)
            Y_pos2 = np.linspace(start=0.25, stop=2.75, num=6)

            for i in range(0, 9):
                for j in range(0, 6):

                    if occupation[i, j] == 1:

                        position = np.array([X_pos2[i],Y_pos2[j]])
                        desired_direction = np.array([-1,0])

                        agent = Agent(uid, position, desired_direction, from_zero, adapt, turn)
                        self.agents.append(agent)
                        uid = uid + 1


    def create_borders(self):
        """ Creates borders defined in a file called by Constants.py"""

        for index, row in cs.BORDERS.iterrows():
            uid = index

            if row["shape"] == "line":

                x1 = row["x1"]
                y1 = row["y1"]
                x2 = row["x2"]
                y2 = row["y2"]

                Qx = np.linspace(start=x1, stop=x2, num=100)
                Qy = np.linspace(start=y1, stop=y2, num=100)

            border = Border(uid, Qx, Qy)
            self.borders.append(border)

    def get_borders(self):
        """ Returns all borders currently in the model."""
        return self.borders

    def get_agents(self):
        """Returns all agents currently in the model."""
        return self.agents

    def create_log(self, log_path):
        """Creates a global logfile and opens it."""

        filename = log_path + "tracks.csv"

        if os.path.exists(filename):
            os.remove(filename)

        csvfile = open(filename, 'a')
        global log
        log = csv.writer(csvfile, delimiter=",")

        attributes = ['agent_id',
            'step',
            'position_x',
            'position_y',
            'velocity_x',
            'velocity_y',
            'resultant_x',
            'resultant_y',
            'time_relaxation_x',
            'time_relaxation_y',
            'agent_repulsion_x',
            'agent_repulsion_y',
            "border_repulsion_x",
            "border_repulsion_y"]

        log.writerow(attributes)

    def calc_mags(self, log_path):
        """Calculates magnitudes of resultant, velocity, border_repulsion, agent_repulsion and time_relaxation"""

        df = pd.read_csv(log_path + "tracks.csv", sep = ",")

        resultant_mag = []
        agent_repulsion_mag = []
        velocity_mag = []
        time_relaxation_mag = []
        border_repulsion_mag = []

        for i in range(len(df)):
            resultant = np.array([df["resultant_x"][i], df["resultant_y"][i]])
            resultant_mag.append(aux.norm(resultant))

            agent_repulsion = np.array([df["agent_repulsion_x"][i], df["agent_repulsion_y"][i]])
            agent_repulsion_mag.append(aux.norm(agent_repulsion))

            velocity = np.array([df["velocity_x"][i], df["velocity_y"][i]])
            velocity_mag.append(aux.norm(velocity))

            time_relaxation = np.array([df["time_relaxation_x"][i], df["time_relaxation_y"][i]])
            time_relaxation_mag.append(aux.norm(time_relaxation))

            border_repulsion = np.array([df["border_repulsion_x"][i], df["border_repulsion_y"][i]])
            border_repulsion_mag.append(aux.norm(border_repulsion))

        df["resultant_mag"] = resultant_mag
        df["velocity_mag"] = velocity_mag
        df["agent_repulsion_mag"] = agent_repulsion_mag
        df["time_relaxation_mag"] = time_relaxation_mag
        df["border_repulsion_mag"] = border_repulsion_mag

        df.to_csv(log_path + "tracks.csv", sep=",", index=False)

    def angle_of_view(self, e, f):

        norm_f = aux.norm(f)
        w = np.dot(e, f)

        if w > norm_f*np.cos(cs.PHI):
            return cs.C
        else:
            return 1

    def graph_gen(self):

        G = nx.Graph()

        for agent in self.agents:
            G.add_node(agent.uid)
            return G

    def is_active(self, agent):
        '''This function will return an agent's status, to test if it should be moved '''

        return agent.status

    def evaluate(self, step):
        '''This function evaluates movement possibilities for all agents.'''
        #global G
        #G = self.graph_gen()

        np.random.shuffle(self.agents)
        for agent in self.agents:

            if self.is_active(agent):

                tot_agent_repulsion = np.array([0, 0]) # f_alphabeta
                tot_border_repulsion = np.array([0, 0]) # F_alphaB
                group_attraction = np.array([0, 0]) # f_alphai
                time_relaxation = np.array([0, 0]) # F_alphazero

                if agent.adapt == True:
                    ex, ey = agent.get_desired_direction()
                else:
                    ex, ey = agent.desired_direction

                for neighbor in self.agents:

                    if self.is_active(neighbor):

                        if neighbor.uid != agent.uid:

                            dist = agent.position - neighbor.position
                            norm_dist = aux.norm(dist)

                            if norm_dist <= cs.DIST_THRESH:

                                #agent_repulsion = agent.agent_repulsion(dist[0], dist[1], v_beta, agent.velocity)
                                agent_repulsion = agent.calc_agent_repulsion(dist[0], dist[1], neighbor.velocity, agent.velocity)

                                angle_of_view = self.angle_of_view(np.array([ex, ey]), agent_repulsion)
                                agent_repulsion = agent_repulsion*angle_of_view

                                agent_repulsion_mag = aux.norm(agent_repulsion)

                                #if agent_repulsion_mag >= 0.1:
                                #    G.add_edge(agent.uid, neighbor.uid)

                                tot_agent_repulsion = agent_repulsion + tot_agent_repulsion


                            else:
                                tot_agent_repulsion = [0, 0] + tot_agent_repulsion


                for border in self.borders:

                    border_dist_min = border.calc_dist(agent.position[0], agent.position[1], border.uid)
                    dist_norm = aux.norm(border_dist_min)

                    if dist_norm <= cs.DIST_THRESH:
                        #border_repulsion = agent.border_repulsion(border_dist_min[0], border_dist_min[1])
                        border_repulsion = agent.calc_border_repulsion(border_dist_min[0], border_dist_min[1])

                        #angle_of_view = self.angle_of_view(np.array([ex, ey]), border_repulsion)
                        #border_repulsion = border_repulsion*angle_of_view

                    else:
                        border_repulsion = [0, 0]

                    tot_border_repulsion = tot_border_repulsion + border_repulsion

                desired_velocity = np.array([agent.desired_speed*ex, agent.desired_speed*ey])
                time_relaxation = (desired_velocity - agent.velocity)/cs.TAU

                #rand = np.random.normal(loc = FLUCT_loc, scale = FLUCT_scale, size = 2)
                rand = [0, 0]
                resultant = np.sum([time_relaxation, tot_agent_repulsion, tot_border_repulsion, group_attraction, rand], axis=0)

                #resultant = [1,1]
                agent.resultant = resultant

                row = [agent.uid,
                    step,
                    agent.position[0],
                    agent.position[1],
                    agent.velocity[0],
                    agent.velocity[1],
                    resultant[0],
                    resultant[1],
                    time_relaxation[0],
                    time_relaxation[1],
                    tot_agent_repulsion[0],
                    tot_agent_repulsion[1],
                    tot_border_repulsion[0],
                    tot_border_repulsion[1]]

            else:

                row = [agent.uid,
                    step,
                    agent.position[0],
                    agent.position[1],
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            log.writerow(row)

            agent.track.append(row)

    def move(self):
        '''This implements agent movement.'''

        for agent in self.agents:

            if self.is_active(agent):

                a = agent.resultant
                s = agent.position
                v = agent.velocity

                new_v = v + a*cs.delta_t
                new_s = s + new_v*cs.delta_t

                new_speed = aux.norm(new_v)

                if new_speed > agent.max_speed:
                    new_v = (new_v*agent.max_speed)/new_speed

                new_x = new_s[0]

                if new_x > cs.X:
                    #new_s[0] = 0
                    agent.status = False

                if new_x < 0:
                    #new_s[0] = cs.X
                    agent.status = False

                new_y = new_s[1]

                if new_y > cs.Y:
                    #new_s[1] = cs.Y
                    #new_v = -new_v
                    agent.status = False

                if new_y < 0:
                    #new_s[1] = 0
                    #new_v = -new_v
                    agent.status = False

                agent.position = new_s
                agent.velocity = new_v

                # If an agent is adaptive and has reached a certain neighborhood of its goal, then inactivate it:
                if agent.adapt == True:

                    dist_to_goal = agent.position - agent.desired_position
                    dist_to_goal = aux.norm(dist_to_goal)

                    if dist_to_goal <= 0.8:

                        agent.status = False


    def run(self, folder_name):
        ''' The the model for N agents and M time steps. '''

        self.create_agents()
        self.create_borders()

        folder = "./log/" + now.strftime("%m%d")

        if not os.path.exists(folder):
            os.mkdir(folder)

        log_path = folder + "/" + folder_name + "/"

        if not os.path.exists(log_path):
            os.mkdir(log_path)

        print("Log will be created in " + log_path)

        self.create_log(log_path)

        for step in range(cs.M):

            print("Step: " + str(step))

            self.evaluate(step)
            self.move()

            # if not os.path.exists(log_path + "graphs/"):
            #     os.mkdir(log_path + "graphs/")

            # graph_name = log_path + "graphs/G" + str(step) + ".adjlist"

            # fh = open(graph_name, "wb")
            # nx.write_adjlist(G, fh)

        # df = self.create_log()
        # df.to_csv(log_path + "tracks.csv")

        self.calc_mags(log_path)

        os.system('cp constants.py ' + log_path + 'constants.py')

        viz.get_viz(log_path, self.borders)

        print("Done!")

start = time.time()


#####################################################
# Uncomment to run whole experiment                ##
#####################################################

import multiprocessing as mp

sens_test = [2.1, 2.1, 2.1, 2.1]
experiment = ["5A", "5B", "5C", "5D"]

def go(exp):
    i = 1
    for test_value in sens_test:

        cs.V_ZERO = test_value

        dt_string = "run" + str(i) + "_sim_S"+ str(cs.M) + "A" + str(cs.N) + "_" + "V_ZERO" + str(cs.V_ZERO) + "cs" + exp

        print("Starting simulation " + dt_string)

        sim = Model(exp)
        sim.run(dt_string)
        i = i + 1

if __name__ == '__main__':
    pool = mp.Pool(mp.cpu_count())
    pool.map(go, [exp for exp in experiment])
    pool.close()


#####################################################
# Uncomment for V_ZERO sensitivity                 ##
#####################################################

# import multiprocessing as mp

# sens_test = [2.1, 21, 210, 2100]

# def go(test_value):

#     exp = "2"

#     cs.V_ZERO = test_value

#     dt_string = "sim_S"+ str(cs.M) + "A" + str(cs.N) + "_" + "V_ZERO" + str(cs.V_ZERO) + "cs" + exp

#     print("starting simulation" + dt_string)

#     sim = Model(exp)
#     sim.run(dt_string)

# if __name__ == '__main__':
#     pool = mp.Pool(mp.cpu_count())
#     pool.map(go, [test_value for test_value in sens_test])
#     pool.close()


#####################################################
# Uncomment for TAU sensitivity                    ##
#####################################################

# import multiprocessing as mp

# sens_test = [0.5, 0.4, 0.3, 0.2, 0.1]

# def go(test_value):

#     exp = "2"

#     cs.TAU = test_value

#     dt_string = "sim_S"+ str(cs.M) + "A" + str(cs.N) + "_" + "TAU" + str(cs.TAU) + "_deltat" + str(cs.Rt) + "_cs" + exp

#     print("starting simulation" + dt_string)

#     sim = Model(exp)
#     sim.run(dt_string)

# if __name__ == '__main__':
#     pool = mp.Pool(mp.cpu_count())
#     pool.map(go, [test_value for test_value in sens_test])
#     pool.close()

######################################################
## Uncomment for TAU vs, V_zero sensitivity         ##
######################################################

## TAU sensitivity is run for two different values of V_ZERO that produce disturbance to agent's intended trajectory.

# sens_test = [0.5, 0.4, 0.3, 0.2, 0.1]
# v_zeros = [210, 2100]

# for v_zero in v_zeros:
#     cs.V_ZERO = v_zero
#     for test in sens_test:
#         cs.TAU = test
#         print("Starting simulation for TAU = " + str(cs.TAU) + " V_ZERO = " + str(cs.V_ZERO))
#         sim = Model()
#         sim.run()


######################################################
## Uncomment for TAU scenarios sensitivity         ##
######################################################

## TAU sensitivity is run for two different values of V_ZERO that produce disturbance to agent's intended trajectory.

# sens_test = [0.5, 0.4, 0.3, 0.2, 0.1]

# for test in sens_test:
#     cs.TAU = test
#     print("Starting simulation for TAU = " + str(cs.TAU))
#     sim = Model()
#     sim.run()


######################################################
## Uncomment for U_zero sensitivity                 ##
######################################################

#Parallelizing using Pool.apply()

# import multiprocessing as mp

# sens_test = [10, 100, 1000]

# def go(test_value):

#     cs.U_ZERO = test_value

#     print("starting simulation")

#     dt_string = "sim_S"+ str(cs.M) + "A" + str(cs.N) + "_" + "U_ZERO" + str(cs.U_ZERO)
#     sim = Model()
#     sim.run(dt_string)

# if __name__ == '__main__':
#     pool = mp.Pool(mp.cpu_count())
#     pool.map(go, [test for test in sens_test])
#     pool.close()

######################################################
## Single Run                                       ##
# ######################################################

# filename = "sim_S"+ str(cs.M) + "A" + str(cs.N) + "_" + "V_ZERO" + str(cs.V_ZERO) + "sc3"
# sim = Model("3")
# sim.run(filename)

######################################################
end = time.time()
runtime = end - start
print(runtime)
