import numpy as np

def norm(array):
    return np.sqrt(array[0]**2 + array[1]**2)
