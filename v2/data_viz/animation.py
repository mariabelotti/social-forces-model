from matplotlib.animation import FuncAnimation

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import os
import constants as cs
from border import Border


class Dot():
    def __init__(self, df, i, N):

        self.subset = df[df["agent_id"] == i]

        first_step = self.subset[self.subset["step"] == 1]
        group = first_step[first_step["step"] == 1]["group"]
        group = bool(group.values)

        if group == 0:
            self.color = "maroon"
        else:
            self.color = "navy"

        self.x = first_step["position_x"]
        self.y = first_step["position_y"]
        self.speed = float(first_step["velocity_mag"].values)

        #cmap = matplotlib.cm.get_cmap("tab20c")

        #self.color = cmap(i/cs.N)

    def move(self, f):
        step = self.subset[self.subset["step"] == f+1]
        new_pos = step.loc[:,"position_x":"position_y"].values
        self.x = new_pos[0,0]
        self.y = new_pos[0,1]
        self.speed = float(step["velocity_mag"].values)


def initialize_borders():

    borders = []
    cs.BORDERS = pd.read_csv("./borders_long.csv")

    for index, row in cs.BORDERS.iterrows():
        uid = index

        if row["shape"] == "line":

            x1 = row["x1"]
            y1 = row["y1"]
            x2 = row["x2"]
            y2 = row["y2"]

            Qx = np.linspace(start=x1, stop=x2, num=50)
            Qy = np.linspace(start=y1, stop=y2, num=50)

        borders.append(Border(uid, Qx, Qy))

    return borders

def get_step_count(df):
    '''Returns the number of steps on current dataset.'''
    return df["agent_id"].value_counts().max()


def animate(f):
    """This function will be called by matplotlib to create each frames."""
    try:
        speeds = []
        positions = []
        colors = []
        for dot in dots:
            # we will move the dot to the next step:
            dot.move(f)
            positions.append([dot.x, dot.y])
            colors.append(dot.color)
            #speeds.append(dot.speed*80)
            speeds.append(30)
        positions = np.array(positions)

        d.set_offsets(positions)
        d.set_edgecolors(colors)
        d.set_facecolors(colors)
        d.set_sizes(speeds)
        time_text.set_text(time_template%(f))


    except IndexError:
        print("There was an Index Error, continuing anyway.")


def create_animation(sim_code):

    log_path = "./log/experiment4/" + sim_code + "/"
    log_file = log_path + "tracks_grouped.csv"

    df = pd.read_csv(log_file)
    df = df[df.step != 0]

    number_of_steps = get_step_count(df)

    N = len(df[df["step"] == 1])

    global dots
    dots = []

    i = 0
    for i in range(N):
        dots.append(Dot(df, i, N))
        i = i + 1

    borders = initialize_borders()

    # First set up the figure, the axis, and the plot element we want to animate
    fig = plt.figure()

    ax = plt.axes(xlim=(0, cs.X), ylim=(0, cs.Y))
    # ax.set_xlim(350, 650)
    # ax.set_ylim(310,335)

    # Add initial blank dots (gambiarra):
    for dot in dots:
        global d
        d = ax.scatter(dot.x, dot.y, color = 'white', s = 5)

    # Plot borders:
    for border in borders:
        Q = np.array([border.Qx, border.Qy]).T
        ax.plot(Q[:,0], Q[:,1], linewidth = 2, linestyle = '-', color = "black")

    # Plot measurement area:

    ax.vlines(x=14, ymin=0, ymax=3)
    ax.vlines(x=24, ymin=0, ymax=3)

    global time_text
    global time_template

    # Display the current frame:
    time_template = 'Time = %.1f'
    time_text = ax.text(1, 2.7, '')

    # call the animator
    anim = FuncAnimation(fig, animate, frames=number_of_steps, interval= 10)

    filename = log_path + "animate.mp4"
    anim.save(filename, writer='ffmpeg', dpi=300)



codes  = ["V_ZERO2/run3_sim_S3500A54_V_ZERO2.1cs5B","V_ZERO2/run3_sim_S3500A54_V_ZERO2.1cs5C", "V_ZERO2/run3_sim_S3500A54_V_ZERO2.1cs5D"]
#codes  = ["V_ZERO2/run3_sim_S3500A54_V_ZERO2.1cs5B"]


#file_list = ["sim_S1500A60_V_ZERO2.1sc5"]
# file_list.remove("sim_S4000A320_V_ZERO21sc4")
# print(file_list)
#file_list = ["sim_S5000A40_V_ZERO2.1cs3", "sim_S5000A40_V_ZERO21cs3", "sim_S5000A40_V_ZERO210cs3", "sim_S5000A40_V_ZERO2100cs3"]

for sim_code in codes:
    print("starting animation: " + sim_code)
    create_animation(sim_code)
